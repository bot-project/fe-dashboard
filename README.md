# Running Spring Boot ##

`mvn clean package` 

## Intellij ##

`java -jar target/bidding-1.0.jar` (Default)

`java -jar -Dspring.profiles.active=stag target/bidding-1.0.jar` (Staging)

`java -jar -Dspring.profiles.active=prod target/bidding-1.0.jar` (Prod)

## Service in Linux ##
1. Create config file : **kuis-1.0.conf** in the same directory **kuis-1.0.jar**
2. Fill this : `JAVA_OPTS="-Dspring.profiles.active=prod"`
3. Create link bidding-1.0.jar to service init.d  : `sudo ln -s target/bidding-1.0.jar /etc/init.d/biddingapp`

4. Start/Stop Service

`sudo /etc/init.d/biddingapp start`

`sudo /etc/init.d/biddingapp stop`

## Domain/IP
1. http://lelang.truemoney.co.id/ (Prod)
2. 172.16.10.136:8080 (Prod)
3. http://172.16.50.136:8558/ (Staging)