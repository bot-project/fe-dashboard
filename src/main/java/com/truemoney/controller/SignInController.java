//package com.truemoney.controller;
//
//import com.truemoney.base.dto.SessionDto;
//import com.truemoney.base.dto.response.LoginResponse;
//import com.truemoney.base.service.LoginService;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.servlet.mvc.support.RedirectAttributes;
//import org.springframework.web.servlet.view.RedirectView;
//
//import javax.servlet.http.HttpSession;
//import java.util.Map;
//
//@Controller
//public class SignInController {
//    private static final Logger logger = LoggerFactory.getLogger(SignInController.class);
//
//    @Autowired
//    LoginService loginService;
//
//    @Value("${portal_url}")
//    String portalUrl;
//
//    @RequestMapping(value={"/login"})
//    public String login(Map<String, Object> model, HttpSession session,
//                       RedirectAttributes redirectAttributes) {
//        SessionDto sessionDto = (SessionDto) session.getAttribute("sessionDto");
//        if(sessionDto != null){
//            return "redirect:/dashboard";
//        }
//        return "login";
//    }
//
//    @RequestMapping("/submitSignin")
//    public String submitSignin(Map<String, Object> model, HttpSession session,
//                               @RequestParam(value = "username") String username,
//                               @RequestParam(value = "password") String password,
//                               RedirectAttributes redirectAttributes) {
//        if(username != null && password != null){
//            LoginResponse loginResponse = loginService.submitLogin(username, password);
//            if(loginResponse != null){
//                if(loginResponse.getRc() != null && loginResponse.getRc().equalsIgnoreCase("000")){
//                    SessionDto sessionDto = new SessionDto();
//                    sessionDto.setUsername(loginResponse.getUsername());
//                    sessionDto.setNama(loginResponse.getNama());
//                    session.setAttribute("sessionDto", sessionDto);
//
//                    redirectAttributes.addFlashAttribute("successMessage", loginResponse.getPesan());
//                    return "redirect:/dashboard";
//                }else{
//                    redirectAttributes.addFlashAttribute("errorMessage", "Silakan coba lagi beberapa saat");
//                }
//            }else{
//                redirectAttributes.addFlashAttribute("errorMessage", "Silakan coba lagi beberapa saat");
//            }
//            return "redirect:/login";
//        }
//        redirectAttributes.addFlashAttribute("errorMessage", "Username dan Password Kosong");
//        return "redirect:/login";
//    }
//
//    @RequestMapping("/logout")
//    public Object signout(Map<String, Object> model, HttpSession session,
//                          RedirectAttributes redirectAttributes) {
//        session.removeAttribute("sessionDto");
//        redirectAttributes.addFlashAttribute("warningMessage", "Silakan login terlebih dahulu");
//        return "redirect:/login";
//    }
//}
