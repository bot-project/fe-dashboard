package com.truemoney.controller;

import com.truemoney.base.dto.SessionDto;
import com.truemoney.base.dto.response.BasicResponse;
import com.truemoney.base.dto.response.DashboardResponse;
import com.truemoney.base.dto.response.FeeSupplierResponse;
import com.truemoney.base.service.DashboardService;
import com.truemoney.base.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
public class ProductController {
    private static final Logger logger = LoggerFactory.getLogger(ProductController.class);

    @Autowired
    ProductService productService;

    @RequestMapping(value={"/list_feesupplier"})
    public Object home(Map<String, Object> model, HttpSession session,
                       RedirectAttributes redirectAttributes) {
        SessionDto sessionDto = (SessionDto) session.getAttribute("sessionDto");
        if(sessionDto != null){
            FeeSupplierResponse data = productService.getData();
            if(data != null && data.getRc() != null && data.getRc().equalsIgnoreCase("000")){
                model.put("data", data);
            }
            return "fee_supplier";
        }
        redirectAttributes.addFlashAttribute("warningMessage", "Mohon untuk login terlebih dahulu");
        return "redirect:/login";
    }

    @RequestMapping(value={"/get_feesupplier/{id}"})
    public Object getFeeSupplier(Map<String, Object> model, HttpSession session,
                                 RedirectAttributes redirectAttributes, @PathVariable("id") String id) {
        SessionDto sessionDto = (SessionDto) session.getAttribute("sessionDto");
        if(sessionDto != null){
            FeeSupplierResponse.FeeSupplierDto data = productService.getData(id);
            if(data != null && data.getRc() != null && data.getRc().equalsIgnoreCase("000")){
                model.put("data", data);
            }else{
                model.put("data", new FeeSupplierResponse.FeeSupplierDto());
            }
            return "fee_supplier_modal";
        }
        redirectAttributes.addFlashAttribute("warningMessage", "Mohon untuk login terlebih dahulu");
        return "redirect:/login";
    }

    @RequestMapping(value={"/update_feesupplier"})
    public Object updateFeeSupplier(Map<String, Object> model, HttpSession session,
                                    @ModelAttribute FeeSupplierResponse.FeeSupplierDto feeSupplierDto,
                                    RedirectAttributes redirectAttributes) {
        SessionDto sessionDto = (SessionDto) session.getAttribute("sessionDto");
        if(sessionDto != null){
            BasicResponse data = productService.updateData(feeSupplierDto);
            if(data != null && data.getRc() != null && data.getRc().equalsIgnoreCase("000")){
                redirectAttributes.addFlashAttribute("succesMessage", "Update berhasil");
                return "redirect:/list_feesupplier";
            }else{
                redirectAttributes.addFlashAttribute("errorMessage", "Update gagal, silakan coba lagi");
                return "redirect:/list_feesupplier";
            }
        }
        redirectAttributes.addFlashAttribute("warningMessage", "Mohon untuk login terlebih dahulu");
        return "redirect:/login";
    }
}