package com.truemoney.controller;

import com.truemoney.base.dto.SessionDto;
import com.truemoney.base.dto.response.DashboardResponse;
import com.truemoney.base.service.DashboardService;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

@Controller
public class DashboardController {
    private static final Logger logger = LoggerFactory.getLogger(DashboardController.class);

    @Autowired
    DashboardService dashboardService;

    @GetMapping(value="/home")
    public Object home(Map<String, Object> model,
                       RedirectAttributes redirectAttributes,
                       @RequestParam(value = "start_date", required = false) String start_date,
                       @RequestParam(value = "end_date", required = false) String end_date) {
//        SessionDto sessionDto = (SessionDto) session.getAttribute("sessionDto");
//        if(sessionDto != null){
            if(start_date == null || end_date == null){
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                start_date = format.format(new Date());
                end_date = format.format(new Date());
            }

            DashboardResponse data = dashboardService.getData(start_date, end_date);
            if(data != null && data.getRc() != null && data.getRc().equalsIgnoreCase("000")){
                model.put("data", data);
            }
            model.put("start_date", start_date);
            model.put("end_date", end_date);
            return "dashboard";
//        }
//        redirectAttributes.addFlashAttribute("warningMessage", "Mohon untuk login terlebih dahulu");
//        return "redirect:/login";
    }
}