package com.truemoney.controller;

import com.truemoney.base.dto.SessionDto;
import com.truemoney.base.dto.response.AmsUpdateStatusResponse;
import com.truemoney.base.dto.response.AmsViewProfileResponse;
import com.truemoney.base.dto.response.DashboardResponse;
import com.truemoney.base.service.AmsViewProfileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import java.util.Map;

@Controller
public class AmsController {
    private static final Logger logger = LoggerFactory.getLogger(AmsController.class);

    @Autowired
    AmsViewProfileService amsViewProfileService;

    @RequestMapping(value={"/view_profile"})
    public Object view_profile(Map<String, Object> model, HttpSession session,
                       RedirectAttributes redirectAttributes,
                       @RequestParam(value = "no_hp", required = false) String no_hp){
        SessionDto sessionDto = (SessionDto) session.getAttribute("sessionDto");
        if(sessionDto != null){
            AmsViewProfileResponse data = amsViewProfileService.getData(no_hp);
            if(data != null && data.getRc() != null && data.getRc().equalsIgnoreCase("000")){
                model.put("data", data);
            }else {
                model.put("data", new AmsViewProfileResponse());
            }
            model.put("no_hp", no_hp);
            return "view_profile";

            }
        redirectAttributes.addFlashAttribute("warningMessage", "Mohon untuk login terlebih dahulu");
        return "redirect:/login";
    }

    @RequestMapping(value={"/update_status"})
    public Object verifikasi(Map<String, Object> model, HttpSession session,
                               RedirectAttributes redirectAttributes,
                               @RequestParam(value = "handphone", required = false) String handphone,
                               @RequestParam(value = "id_memberaccount", required = false) String id_memberaccount,
                               @RequestParam(value = "status_verifikasi", required = false) String status_verifikasi){
        SessionDto sessionDto = (SessionDto) session.getAttribute("sessionDto");
        if(sessionDto != null){
            AmsUpdateStatusResponse dataUpdate = amsViewProfileService.getUpdateStatus(id_memberaccount, status_verifikasi);
            redirectAttributes.addFlashAttribute("warningMessage", "Mohon untuk login terlebih dahulu");
            return "redirect:/view_profile?no_hp="+handphone;

        }
        redirectAttributes.addFlashAttribute("warningMessage", "Mohon untuk login terlebih dahulu");
        return "redirect:/login";
    }

}
