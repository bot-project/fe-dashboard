package com.truemoney;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BotWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(BotWebApplication.class, args);
	}
}
