package com.truemoney.base;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.*;

@Configuration
@EnableWebMvc
public class WebMvcConfig implements WebMvcConfigurer {

    @Override
    public void addViewControllers(final ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("redirect:/home");
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler(
                "/webjars/**",
                "/demo/**",
                "/images/**",
                "/fonts/**",
                "/javascripts/**",
                "/stylesheets/**")
                .addResourceLocations(
                        "classpath:/META-INF/resources/webjars/",
                        "classpath:/static/images/",
                        "classpath:/static/vendor/",
                        "classpath:/static/javascripts/",
                        "classpath:/static/stylesheets/",
                        "classpath:/static/fonts/");
    }
}
