package com.truemoney.base.dto;

import java.util.Date;

public class MsBidding {
    private String name;
    private Integer productId;
    private String productName;
    private String accountId;
    private Boolean bidState;
    private Date date;
    private Double amount;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Boolean getBidState() {
        return bidState;
    }

    public void setBidState(Boolean bidState) {
        this.bidState = bidState;
    }
}
