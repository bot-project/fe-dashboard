package com.truemoney.base.dto;

import java.util.ArrayList;
import java.util.List;

public class MsBidProductDetailDto extends MsBidProductDto {
    private List<MsBidding> bidders = new ArrayList<>();
    private double lastBid;
    private String status;
    private String type;
    private Long amount;

    public MsBidProductDetailDto(){
        super();
    }

    public List<MsBidding> getBidders() {
        return bidders;
    }

    public void setBidders(List<MsBidding> bidders) {
        this.bidders = bidders;
    }

    public double getLastBid() {
        return lastBid;
    }

    public void setLastBid(double lastBid) {
        this.lastBid = lastBid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }
}
