package com.truemoney.base.dto.request;

public class AmsViewProfileRequest {
    String no_hp;

    public String getNo_hp() {
        return no_hp;
    }

    public void setNo_hp(String no_hp) {
        this.no_hp = no_hp;
    }
}
