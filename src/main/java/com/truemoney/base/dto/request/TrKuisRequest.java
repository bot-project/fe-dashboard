package com.truemoney.base.dto.request;

public class TrKuisRequest {
    Integer idPertanyaan;
    Integer idJawaban;
    String idAccount;
    String type;

    public Integer getIdPertanyaan() {
        return idPertanyaan;
    }

    public void setIdPertanyaan(Integer idPertanyaan) {
        this.idPertanyaan = idPertanyaan;
    }

    public Integer getIdJawaban() {
        return idJawaban;
    }

    public void setIdJawaban(Integer idJawaban) {
        this.idJawaban = idJawaban;
    }

    public String getIdAccount() {
        return idAccount;
    }

    public void setIdAccount(String idAccount) {
        this.idAccount = idAccount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
