package com.truemoney.base.dto.request;

public class PeriodRequest {
    private String periodCode;

    public String getPeriodCode() {
        return periodCode;
    }

    public void setPeriodCode(String periodCode) {
        this.periodCode = periodCode;
    }
}
