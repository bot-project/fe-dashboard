package com.truemoney.base.dto.request;

import java.sql.Timestamp;

public class TrKuisRedemptionRequest extends BasicRequest{
    private Integer idRedemption;
    private String idAccount;
    private Integer idBarang;
    private Timestamp timestamp;

    public Integer getIdRedemption() {
        return idRedemption;
    }

    public void setIdRedemption(Integer idRedemption) {
        this.idRedemption = idRedemption;
    }

    public String getIdAccount() {
        return idAccount;
    }

    public void setIdAccount(String idAccount) {
        this.idAccount = idAccount;
    }

    public Integer getIdBarang() {
        return idBarang;
    }

    public void setIdBarang(Integer idBarang) {
        this.idBarang = idBarang;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }
}
