package com.truemoney.base.dto.request;

public class FeeSupplierRequest {
    String id_feesupplier;

    public String getId_feesupplier() {
        return id_feesupplier;
    }

    public void setId_feesupplier(String id_feesupplier) {
        this.id_feesupplier = id_feesupplier;
    }
}
