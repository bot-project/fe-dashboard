package com.truemoney.base.dto.request;

public class AmsUpdateStatusRequest {
    String id_memberaccount;
    String status_verifikasi;

    public String getId_memberaccount() {
        return id_memberaccount;
    }

    public void setId_memberaccount(String id_memberaccount) {
        this.id_memberaccount = id_memberaccount;
    }

    public String getStatus_verifikasi() {
        return status_verifikasi;
    }

    public void setStatus_verifikasi(String status_verifikasi) {
        this.status_verifikasi = status_verifikasi;
    }
}
