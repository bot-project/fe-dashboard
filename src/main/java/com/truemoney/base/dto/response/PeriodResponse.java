package com.truemoney.base.dto.response;

import java.sql.Timestamp;

public class PeriodResponse extends BasicResponse {
    private String periodCode;
    private Timestamp startDate;
    private Timestamp endDate;
    private Boolean isEnable;
    private Integer secEvQuest;
    private Integer pointEvQuest;
    private Integer amountEvQuest;

    public String getPeriodCode() {
        return periodCode;
    }

    public void setPeriodCode(String periodCode) {
        this.periodCode = periodCode;
    }

    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    public Timestamp getEndDate() {
        return endDate;
    }

    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }

    public Boolean getEnable() {
        return isEnable;
    }

    public void setEnable(Boolean enable) {
        isEnable = enable;
    }

    public Integer getSecEvQuest() {
        return secEvQuest;
    }

    public void setSecEvQuest(Integer secEvQuest) {
        this.secEvQuest = secEvQuest;
    }

    public Integer getPointEvQuest() {
        return pointEvQuest;
    }

    public void setPointEvQuest(Integer pointEvQuest) {
        this.pointEvQuest = pointEvQuest;
    }

    public Integer getAmountEvQuest() {
        return amountEvQuest;
    }

    public void setAmountEvQuest(Integer amountEvQuest) {
        this.amountEvQuest = amountEvQuest;
    }
}
