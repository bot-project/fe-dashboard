package com.truemoney.base.dto.response;

public class StatusResponse extends BasicResponse {
    Boolean accountBidState;

    public Boolean getAccountBidState() {
        return accountBidState;
    }

    public void setAccountBidState(Boolean accountBidState) {
        this.accountBidState = accountBidState;
    }
}
