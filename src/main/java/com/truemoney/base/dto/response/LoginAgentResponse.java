package com.truemoney.base.dto.response;

public class LoginAgentResponse extends BasicResponse{
    String idAgentAccount;
    String phoneNumber;
    String nama;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getIdAgentAccount() {
        return idAgentAccount;
    }

    public void setIdAgentAccount(String idAgentAccount) {
        this.idAgentAccount = idAgentAccount;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
}
