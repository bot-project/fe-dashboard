package com.truemoney.base.dto.response;

import java.util.List;

public class PertanyaanResponse extends BasicResponse {
    private Integer idPertanyaan;
    private String pertanyaan;
    private List<Jawaban> listJawaban;

    public Integer getIdPertanyaan() {
        return idPertanyaan;
    }

    public void setIdPertanyaan(Integer idPertanyaan) {
        this.idPertanyaan = idPertanyaan;
    }

    public String getPertanyaan() {
        return pertanyaan;
    }

    public void setPertanyaan(String pertanyaan) {
        this.pertanyaan = pertanyaan;
    }

    public List<Jawaban> getListJawaban() {
        return listJawaban;
    }

    public void setListJawaban(List<Jawaban> listJawaban) {
        this.listJawaban = listJawaban;
    }

    public static class Jawaban{
        private Integer idJawaban;
        private String jawaban;
        private Boolean isAnswer;

        public Integer getIdJawaban() {
            return idJawaban;
        }

        public void setIdJawaban(Integer idJawaban) {
            this.idJawaban = idJawaban;
        }

        public String getJawaban() {
            return jawaban;
        }

        public void setJawaban(String jawaban) {
            this.jawaban = jawaban;
        }

        public Boolean getAnswer() {
            return isAnswer;
        }

        public void setAnswer(Boolean answer) {
            isAnswer = answer;
        }
    }
}
