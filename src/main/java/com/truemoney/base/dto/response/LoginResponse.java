package com.truemoney.base.dto.response;

import java.util.List;

public class LoginResponse extends BasicResponse{
    String username;
    String nama;

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
