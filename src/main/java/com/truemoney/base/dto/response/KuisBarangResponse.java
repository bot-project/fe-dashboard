package com.truemoney.base.dto.response;

import com.truemoney.base.dto.KuisBarangtDto;

import java.util.List;

public class KuisBarangResponse extends BasicResponse {
    private KuisBarangtDto kuisBarang;
    private List<KuisBarangtDto> listKuisBarang;

    public KuisBarangtDto getKuisBarang() {
        return kuisBarang;
    }

    public void setKuisBarang(KuisBarangtDto kuisBarang) {
        this.kuisBarang = kuisBarang;
    }

    public List<KuisBarangtDto> getListKuisBarang() {
        return listKuisBarang;
    }

    public void setListKuisBarang(List<KuisBarangtDto> listKuisBarang) {
        this.listKuisBarang = listKuisBarang;
    }
}
