package com.truemoney.base.dto.response;

import com.truemoney.base.dto.TrKuisRedemptionDto;

import java.util.List;

public class TrKuisRedemptionResponse extends BasicResponse {
    List<TrKuisRedemptionDto> listRedemption;

    public List<TrKuisRedemptionDto> getListRedemption() {
        return listRedemption;
    }

    public void setListRedemption(List<TrKuisRedemptionDto> listRedemption) {
        this.listRedemption = listRedemption;
    }
}
