package com.truemoney.base.dto.response;

public class TrKuisResponse extends BasicResponse {
    Boolean isAnswer;
    Integer point;

    public Boolean getAnswer() {
        return isAnswer;
    }

    public void setAnswer(Boolean answer) {
        isAnswer = answer;
    }

    public Integer getPoint() {
        return point;
    }

    public void setPoint(Integer point) {
        this.point = point;
    }
}
