package com.truemoney.base.dto.response;

import java.util.List;

public class AmsViewProfileResponse {
    String msg;
    String rc;
    Profile profile;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getRc() {
        return rc;
    }

    public void setRc(String rc) {
        this.rc = rc;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    static class Profile {
        String nama_member;
        String handphone;
        String tanggalDaftar;
        String id_member;
        String jenisKelamin;
        String nomor_kartu;
        String tanggalLahir;
        String namaIbuKandung;
        String id_statusverifikasi;
        String nomorKtp;
        String alamat;
        String kota;
        String kelurahan;
        String kecamatan;
        String kodePos;
        String email;
        String telpon;

        public String getNama_member() {
            return nama_member;
        }

        public void setNama_member(String nama_member) {
            this.nama_member = nama_member;
        }

        public String getHandphone() {
            return handphone;
        }

        public void setHandphone(String handphone) {
            this.handphone = handphone;
        }

        public String getTanggalDaftar() {
            return tanggalDaftar;
        }

        public void setTanggalDaftar(String tanggalDaftar) {
            this.tanggalDaftar = tanggalDaftar;
        }

        public String getId_member() {
            return id_member;
        }

        public void setId_member(String id_member) {
            this.id_member = id_member;
        }

        public String getJenisKelamin() {
            return jenisKelamin;
        }

        public void setJenisKelamin(String jenisKelamin) {
            this.jenisKelamin = jenisKelamin;
        }

        public String getNomor_kartu() {
            return nomor_kartu;
        }

        public void setNomor_kartu(String nomor_kartu) {
            this.nomor_kartu = nomor_kartu;
        }

        public String getTanggalLahir() {
            return tanggalLahir;
        }

        public void setTanggalLahir(String tanggalLahir) {
            this.tanggalLahir = tanggalLahir;
        }

        public String getNamaIbuKandung() {
            return namaIbuKandung;
        }

        public void setNamaIbuKandung(String namaIbuKandung) {
            this.namaIbuKandung = namaIbuKandung;
        }

        public String getId_statusverifikasi() {
            return id_statusverifikasi;
        }

        public void setId_statusverifikasi(String id_statusverifikasi) {
            this.id_statusverifikasi = id_statusverifikasi;
        }

        public String getNomorKtp() {
            return nomorKtp;
        }

        public void setNomorKtp(String nomorKtp) {
            this.nomorKtp = nomorKtp;
        }

        public String getAlamat() {
            return alamat;
        }

        public void setAlamat(String alamat) {
            this.alamat = alamat;
        }

        public String getKota() {
            return kota;
        }

        public void setKota(String kota) {
            this.kota = kota;
        }

        public String getKelurahan() {
            return kelurahan;
        }

        public void setKelurahan(String kelurahan) {
            this.kelurahan = kelurahan;
        }

        public String getKecamatan() {
            return kecamatan;
        }

        public void setKecamatan(String kecamatan) {
            this.kecamatan = kecamatan;
        }

        public String getKodePos() {
            return kodePos;
        }

        public void setKodePos(String kodePos) {
            this.kodePos = kodePos;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getTelpon() {
            return telpon;
        }

        public void setTelpon(String telpon) {
            this.telpon = telpon;
        }
    }
}
