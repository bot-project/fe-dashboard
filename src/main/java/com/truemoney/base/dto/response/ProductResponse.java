package com.truemoney.base.dto.response;

import com.truemoney.base.dto.MsBidProductDetailDto;
import com.truemoney.base.dto.MsBidProductDto;

import java.util.List;

public class ProductResponse extends BasicResponse {
    MsBidProductDetailDto productDetail;
    private List<MsBidProductDto> biddingProducts;

    public MsBidProductDetailDto getProductDetail() {
        return productDetail;
    }

    public void setProductDetail(MsBidProductDetailDto productDetail) {
        this.productDetail = productDetail;
    }

    public List<MsBidProductDto> getBiddingProducts() {
        return biddingProducts;
    }

    public void setBiddingProducts(List<MsBidProductDto> biddingProducts) {
        this.biddingProducts = biddingProducts;
    }
}
