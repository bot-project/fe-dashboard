package com.truemoney.base.dto.response;

import java.util.List;

public class DashboardResponse extends BasicResponse{
    String total_transaksi;
    String total_tpv;
    String total_revenue;
    List<TransaksiProduct> transaksi_produk;
    String total_akuisisi;

    public String getTotal_transaksi() {
        return total_transaksi;
    }

    public void setTotal_transaksi(String total_transaksi) {
        this.total_transaksi = total_transaksi;
    }

    public String getTotal_tpv() {
        return total_tpv;
    }

    public void setTotal_tpv(String total_tpv) {
        this.total_tpv = total_tpv;
    }

    public String getTotal_revenue() {
        return total_revenue;
    }

    public void setTotal_revenue(String total_revenue) {
        this.total_revenue = total_revenue;
    }

    public List<TransaksiProduct> getTransaksi_produk() {
        return transaksi_produk;
    }

    public void setTransaksi_produk(List<TransaksiProduct> transaksi_produk) {
        this.transaksi_produk = transaksi_produk;
    }

    public String getTotal_akuisisi() {
        return total_akuisisi;
    }

    public void setTotal_akuisisi(String total_akuisisi) {
        this.total_akuisisi = total_akuisisi;
    }

    static class TransaksiProduct{
        String total;
        String tipe_transaksi;

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public String getTipe_transaksi() {
            return tipe_transaksi;
        }

        public void setTipe_transaksi(String tipe_transaksi) {
            this.tipe_transaksi = tipe_transaksi;
        }
    }
}
