package com.truemoney.base.dto.response;

import java.sql.Timestamp;
import java.util.List;

public class StatisticResponse extends BasicResponse {
    Integer karyawanIkut;
    Integer karyawanTidakIkut;
    Integer karyawanJawabBenar;
    Integer karyawanJawabSalah;
    List<Ranking> rankings;
    List<Pertanyaan> pertanyaan;
    Long totalPertanyaan;

    public Integer getKaryawanIkut() {
        return karyawanIkut;
    }

    public void setKaryawanIkut(Integer karyawanIkut) {
        this.karyawanIkut = karyawanIkut;
    }

    public Integer getKaryawanTidakIkut() {
        return karyawanTidakIkut;
    }

    public void setKaryawanTidakIkut(Integer karyawanTidakIkut) {
        this.karyawanTidakIkut = karyawanTidakIkut;
    }

    public Integer getKaryawanJawabBenar() {
        return karyawanJawabBenar;
    }

    public void setKaryawanJawabBenar(Integer karyawanJawabBenar) {
        this.karyawanJawabBenar = karyawanJawabBenar;
    }

    public Integer getKaryawanJawabSalah() {
        return karyawanJawabSalah;
    }

    public void setKaryawanJawabSalah(Integer karyawanJawabSalah) {
        this.karyawanJawabSalah = karyawanJawabSalah;
    }

    public List<Ranking> getRankings() {
        return rankings;
    }

    public void setRankings(List<Ranking> rankings) {
        this.rankings = rankings;
    }

    public List<Pertanyaan> getPertanyaan() {
        return pertanyaan;
    }

    public void setPertanyaan(List<Pertanyaan> pertanyaan) {
        this.pertanyaan = pertanyaan;
    }

    public Long getTotalPertanyaan() {
        return totalPertanyaan;
    }

    public void setTotalPertanyaan(Long totalPertanyaan) {
        this.totalPertanyaan = totalPertanyaan;
    }

    public static class Ranking {
        String nama;
        Integer point;
        Integer benar;
        Integer salah;
        Timestamp dates;

        public String getNama() {
            return nama;
        }

        public void setNama(String nama) {
            this.nama = nama;
        }

        public Integer getPoint() {
            return point;
        }

        public void setPoint(Integer point) {
            this.point = point;
        }

        public Integer getBenar() {
            return benar;
        }

        public void setBenar(Integer benar) {
            this.benar = benar;
        }

        public Integer getSalah() {
            return salah;
        }

        public void setSalah(Integer salah) {
            this.salah = salah;
        }

        public Timestamp getDates() {
            return dates;
        }

        public void setDates(Timestamp dates) {
            this.dates = dates;
        }
    }

    public static class Pertanyaan{
        Integer idPertanyaan;
        String pertanyaan;
        Integer jawabanBenar;
        Integer jawabanSalah;
        List<String> listBenar;
        List<String> listSalah;

        public String getPertanyaan() {
            return pertanyaan;
        }

        public void setPertanyaan(String pertanyaan) {
            this.pertanyaan = pertanyaan;
        }

        public Integer getJawabanBenar() {
            return jawabanBenar;
        }

        public void setJawabanBenar(Integer jawabanBenar) {
            this.jawabanBenar = jawabanBenar;
        }

        public Integer getJawabanSalah() {
            return jawabanSalah;
        }

        public void setJawabanSalah(Integer jawabanSalah) {
            this.jawabanSalah = jawabanSalah;
        }

        public Integer getIdPertanyaan() {
            return idPertanyaan;
        }

        public void setIdPertanyaan(Integer idPertanyaan) {
            this.idPertanyaan = idPertanyaan;
        }

        public List<String> getListBenar() {
            return listBenar;
        }

        public void setListBenar(List<String> listBenar) {
            this.listBenar = listBenar;
        }

        public List<String> getListSalah() {
            return listSalah;
        }

        public void setListSalah(List<String> listSalah) {
            this.listSalah = listSalah;
        }
    }
}
