package com.truemoney.base.dto.response;

import java.util.List;

public class FeeSupplierResponse extends BasicResponse{
    List<FeeSupplierDto> list_feesupplier;

    public List<FeeSupplierDto> getList_feesupplier() {
        return list_feesupplier;
    }

    public void setList_feesupplier(List<FeeSupplierDto> list_feesupplier) {
        this.list_feesupplier = list_feesupplier;
    }

    public static class FeeSupplierDto extends BasicResponse{
        String nama_operator;
        String nominal;
        String harga_beli;
        Boolean flag_active;
        String id_feesupplier;
        String harga_cetak;
        String harga_cetak_member;
        String product_code;
        String harga_jual;
        String tipe_transaksi;

        public String getNama_operator() {
            return nama_operator;
        }

        public void setNama_operator(String nama_operator) {
            this.nama_operator = nama_operator;
        }

        public String getNominal() {
            return nominal;
        }

        public void setNominal(String nominal) {
            this.nominal = nominal;
        }

        public String getHarga_beli() {
            return harga_beli;
        }

        public void setHarga_beli(String harga_beli) {
            this.harga_beli = harga_beli;
        }

        public Boolean getFlag_active() {
            return flag_active;
        }

        public void setFlag_active(Boolean flag_active) {
            this.flag_active = flag_active;
        }

        public String getId_feesupplier() {
            return id_feesupplier;
        }

        public void setId_feesupplier(String id_feesupplier) {
            this.id_feesupplier = id_feesupplier;
        }

        public String getHarga_cetak() {
            return harga_cetak;
        }

        public void setHarga_cetak(String harga_cetak) {
            this.harga_cetak = harga_cetak;
        }

        public String getHarga_cetak_member() {
            return harga_cetak_member;
        }

        public void setHarga_cetak_member(String harga_cetak_member) {
            this.harga_cetak_member = harga_cetak_member;
        }

        public String getProduct_code() {
            return product_code;
        }

        public void setProduct_code(String product_code) {
            this.product_code = product_code;
        }

        public String getHarga_jual() {
            return harga_jual;
        }

        public void setHarga_jual(String harga_jual) {
            this.harga_jual = harga_jual;
        }

        public String getTipe_transaksi() {
            return tipe_transaksi;
        }

        public void setTipe_transaksi(String tipe_transaksi) {
            this.tipe_transaksi = tipe_transaksi;
        }
    }
}
