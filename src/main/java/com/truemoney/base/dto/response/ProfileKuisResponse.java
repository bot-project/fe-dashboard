package com.truemoney.base.dto.response;

public class ProfileKuisResponse extends BasicResponse{
    Long totalPoint;
    Integer balance;

    public Long getTotalPoint() {
        return totalPoint;
    }

    public void setTotalPoint(Long totalPoint) {
        this.totalPoint = totalPoint;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }
}
