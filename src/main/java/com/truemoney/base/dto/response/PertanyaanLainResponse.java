package com.truemoney.base.dto.response;

import com.truemoney.base.dto.PertanyaanDto;

import java.util.List;

public class PertanyaanLainResponse extends BasicResponse {
    private List<PertanyaanDto> list;

    public List<PertanyaanDto> getList() {
        return list;
    }

    public void setList(List<PertanyaanDto> list) {
        this.list = list;
    }
}
