package com.truemoney.base.dto;

import java.util.Date;
import java.util.List;

public class MsBidProductDto {
    private Integer productId;
    private String name;
    private String desc;
    private Double price;
    private Integer bids;
    private Date dueDate;
    private List<MsBidProductImageDto> productImages;
    public MsBidProductDto(){}

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getBids() {
        return bids;
    }

    public void setBids(Integer bids) {
        this.bids = bids;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public List<MsBidProductImageDto> getProductImages() {
        return productImages;
    }

    public void setProductImages(List<MsBidProductImageDto> productImages) {
        this.productImages = productImages;
    }
}
