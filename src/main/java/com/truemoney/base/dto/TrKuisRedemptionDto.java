package com.truemoney.base.dto;

import java.sql.Timestamp;

public class TrKuisRedemptionDto {
    private Integer idRedemption;
    private String idAccount;
    private MsKuisBarangDto msKuisBarang;
    private Timestamp timestamp;
    private Integer pointRedeemed;

    public Integer getIdRedemption() {
        return idRedemption;
    }

    public void setIdRedemption(Integer idRedemption) {
        this.idRedemption = idRedemption;
    }

    public String getIdAccount() {
        return idAccount;
    }

    public void setIdAccount(String idAccount) {
        this.idAccount = idAccount;
    }

    public MsKuisBarangDto getMsKuisBarang() {
        return msKuisBarang;
    }

    public void setMsKuisBarang(MsKuisBarangDto msKuisBarang) {
        this.msKuisBarang = msKuisBarang;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getPointRedeemed() {
        return pointRedeemed;
    }

    public void setPointRedeemed(Integer pointRedeemed) {
        this.pointRedeemed = pointRedeemed;
    }
}
