package com.truemoney.base.dto;

import com.truemoney.base.dto.response.BasicResponse;

import java.util.List;

public class ProfileDto extends BasicResponse{
    private Long totalPoint;
    private Integer balance;
    private List<TrKuisRedemptionDto> listRedemption;

    public Long getTotalPoint() {
        return totalPoint;
    }

    public void setTotalPoint(Long totalPoint) {
        this.totalPoint = totalPoint;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public List<TrKuisRedemptionDto> getListRedemption() {
        return listRedemption;
    }

    public void setListRedemption(List<TrKuisRedemptionDto> listRedemption) {
        this.listRedemption = listRedemption;
    }
}
