package com.truemoney.base.dto;

import com.truemoney.base.dto.response.BasicResponse;

import java.util.List;

public class ProfileAgentDto extends BasicResponse{
    private String idAgentAccount;
    private String name;
    private Integer lastBalance;
    private List<MsBidding> bids;

    public String getIdAgentAccount() {
        return idAgentAccount;
    }

    public void setIdAgentAccount(String idAgentAccount) {
        this.idAgentAccount = idAgentAccount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLastBalance() {
        return lastBalance;
    }

    public void setLastBalance(Integer lastBalance) {
        this.lastBalance = lastBalance;
    }

    public List<MsBidding> getBids() {
        return bids;
    }

    public void setBids(List<MsBidding> bids) {
        this.bids = bids;
    }
}
