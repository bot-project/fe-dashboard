package com.truemoney.base.function;

public class VariableString {

    public static final String JSON_ERROR = "{\"ack\":\"NOK\", \"pesan\":\"The Connection has timed out\"}";

    public static final String loginPath = "login";
    public static final String submitLogin = "submitLogin";
    public static final String dashboard = "dashboard";
    public static final String view_profile = "view_profile";
    public static final String update_status = "update_status";
    public static final String get_list_feesup = "get_list_feesup";
    public static final String get_feesup = "get_feesup";
    public static final String update_feesup = "update_feesup";

    public static final String biddingBasePath = "bidding";
    public static final String bid = "bid";
    public static final String getProductsPath = "getBidProducts";
    public static final String getProdDetailPath = "getBidProduct";
    public static final String getAccountBidState = "getAccountBidState";
    public static final String getBidProfile = "getBidProfile";
    public static final String getBidProfileAgent = "getBidProfileAgent";


    public static final String getPeriod = "getPeriod";
    public static final String getKuisProfile = "getKuisProfile";
    public static final String getPertanyaan = "getPertanyaan";
    public static final String getOtherPertanyaan = "getOtherPertanyaan";
    public static final String listOtherPertanyaan = "listOtherPertanyaan";
    public static final String saveKuis = "saveKuis";
    public static final String savePertanyaan = "savePertanyaan";
    public static final String saveOtherPertanyaan = "saveOtherPertanyaan";
    public static final String getListBarang = "getListBarang";
    public static final String saveRedemption = "saveRedemption";
    public static final String getStatistic = "getStatistic";

    public static final String OK = "OK";
    public static final String NOK = "NOK";
}
