//package com.truemoney.base.function;
//
//import sun.misc.BASE64Decoder;
//import sun.misc.BASE64Encoder;
//
//import javax.crypto.BadPaddingException;
//import javax.crypto.Cipher;
//import javax.crypto.IllegalBlockSizeException;
//import javax.crypto.NoSuchPaddingException;
//import javax.crypto.spec.SecretKeySpec;
//import java.io.IOException;
//import java.net.URLEncoder;
//import java.security.InvalidKeyException;
//import java.security.Key;
//import java.security.NoSuchAlgorithmException;
//
//public class EncryptDecrypt {
//
//    public String key = "4ss4l4mu4l41kumw4r0hm4tull4h!.wb";
//
//    private byte[] konversiKeByte(String kunci) throws Exception {
//        byte[] array_byte = new byte[32];
//        int i = 0;
//        while (i < kunci.length()) {
//            array_byte[i] = (byte) kunci.charAt(i);
//            i++;
//        } if (i < 32) {
//            while (i < 32) {
//                array_byte[i] = (byte) i; i++;
//            }
//        }
//        return array_byte;
//    }
//
//    private Key generateKey(String kunciEnkripsi) throws Exception {
//        return new SecretKeySpec( konversiKeByte(kunciEnkripsi), "AES");
//    }
//
//    public String encrypt(String text, String kunci) throws Exception {
//        // SecretKeySpec spec = getKeySpec();
//        Key key = generateKey(kunci);
//        Cipher cipher;
//        String encode = "";
//        try {
//            cipher = Cipher.getInstance("AES");
//            cipher.init(Cipher.ENCRYPT_MODE, key);
//            BASE64Encoder enc = new BASE64Encoder();
//            encode = enc.encode(cipher.doFinal(text.getBytes()));
//        } catch (IllegalBlockSizeException e) {
//
//        } catch (BadPaddingException e) {
//
//        } catch (NoSuchPaddingException e) {
//
//        } catch (NoSuchAlgorithmException e) {
//
//        } catch (InvalidKeyException e) {
//
//        } return encode;
//    }
//
//    public String decrypt(String text, String kunci) throws Exception {
//        // SecretKeySpec spec = getKeySpec();
//        String endcode = "";
//        try {
//            Key key = generateKey(kunci);
//            Cipher cipher = Cipher.getInstance("AES");
//            cipher.init(Cipher.DECRYPT_MODE, key);
//            BASE64Decoder dec = new BASE64Decoder();
//            endcode = new String(cipher.doFinal(dec.decodeBuffer(text)));
//        } catch (NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException | IOException e) {
//
//        }
//        return endcode;
//    }
//
//    public static void main(String[] args) throws Exception {
//        EncryptDecrypt encryptDecrypt = new EncryptDecrypt();
//        String enc = encryptDecrypt.encrypt("180209000023", encryptDecrypt.key);
//        String text = URLEncoder.encode("97K+3Hw64md9iOlzq4LK+A==", "UTF-8");
//        System.out.println(text);
//    }
//}
