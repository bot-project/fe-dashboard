//package com.truemoney.base;
//
//import com.truemoney.base.dto.SessionDto;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.web.servlet.HandlerInterceptor;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//public class AuthenticationInterceptor implements HandlerInterceptor {
//    private static final Logger logger = LoggerFactory.getLogger(AuthenticationInterceptor.class);
//
//    @Override
//    public boolean preHandle(
//            HttpServletRequest request,
//            HttpServletResponse response,
//            Object handler) throws Exception {
//        logger.info("intercept preHandle");
//        // Avoid a redirect loop for some urls
//        if(!request.getRequestURI().equals("/signin") &&
//                !request.getRequestURI().equals("/home")) {
//            logger.info("intercept login");
//            SessionDto sessionDto = (SessionDto) request.getSession().getAttribute("sessionLogin");
//            if(sessionDto == null) {
//                response.sendRedirect("/signin");
//                return false;
//            }
//        }
//        return true;
//    }
//}
