package com.truemoney.base.service;

import com.google.gson.Gson;
import com.truemoney.base.dto.request.AmsUpdateStatusRequest;
import com.truemoney.base.dto.request.AmsViewProfileRequest;
import com.truemoney.base.dto.request.DashboardRequest;
import com.truemoney.base.dto.response.AmsUpdateStatusResponse;
import com.truemoney.base.dto.response.AmsViewProfileResponse;
import com.truemoney.base.dto.response.DashboardResponse;
import com.truemoney.base.function.VariableString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AmsViewProfileServiceImpl implements AmsViewProfileService {
    private static final Logger logger = LoggerFactory.getLogger(AmsViewProfileServiceImpl.class);

    @Autowired
    Gson gson;

    @Autowired
    ApiConnectionService apiConnection;

    @Override
    public AmsViewProfileResponse getData(String no_hp) {
        AmsViewProfileRequest request = new AmsViewProfileRequest();
        request.setNo_hp(no_hp);
        String response = apiConnection.sendRequest(gson.toJson(request), VariableString.view_profile);
        return gson.fromJson(response, AmsViewProfileResponse.class);
    }

    @Override
    public AmsUpdateStatusResponse getUpdateStatus(String id_memberaccount, String status_verifikasi) {
        AmsUpdateStatusRequest request = new AmsUpdateStatusRequest();
        request.setId_memberaccount(id_memberaccount);
        request.setStatus_verifikasi(status_verifikasi);
        String response = apiConnection.sendRequest(gson.toJson(request), VariableString.update_status);
        return gson.fromJson(response, AmsUpdateStatusResponse.class);
    }
}
