//package com.truemoney.base.service;
//
//import com.google.gson.Gson;
//import com.truemoney.base.dto.request.LoginRequest;
//import com.truemoney.base.dto.response.LoginAgentResponse;
//import com.truemoney.base.dto.response.LoginResponse;
//import com.truemoney.base.function.VariableString;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.stereotype.Service;
//
//@Service
//public class LoginServiceImpl implements LoginService{
//    private static final Logger logger = LoggerFactory.getLogger(LoginServiceImpl.class);
//
//    @Autowired
//    Gson gson;
//
//    @Autowired
//    ApiConnectionService apiConnection;
//
//    public LoginResponse submitLogin(String username, String password){
//        LoginRequest request = new LoginRequest();
//        request.setUsername(username);
//        request.setPassword(password);
//        String response = apiConnection.sendRequest(gson.toJson(request),
//                VariableString.loginPath+"/"+VariableString.submitLogin);
//        LoginResponse loginResponse = gson.fromJson(response, LoginResponse.class);
//        return loginResponse;
//    }
//}
