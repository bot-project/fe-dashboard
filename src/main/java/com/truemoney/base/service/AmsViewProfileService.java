package com.truemoney.base.service;

import com.truemoney.base.dto.response.AmsUpdateStatusResponse;
import com.truemoney.base.dto.response.AmsViewProfileResponse;

public interface AmsViewProfileService {
    AmsViewProfileResponse getData(String no_hp);
    AmsUpdateStatusResponse getUpdateStatus(String id_memberaccount, String status_verifikasi);

}
