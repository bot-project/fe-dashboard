package com.truemoney.base.service;

import com.google.gson.Gson;
import com.truemoney.base.dto.request.DashboardRequest;
import com.truemoney.base.dto.request.LoginRequest;
import com.truemoney.base.dto.response.DashboardResponse;
import com.truemoney.base.dto.response.LoginResponse;
import com.truemoney.base.function.VariableString;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DashboardServiceImpl implements DashboardService{
    private static final Logger logger = LoggerFactory.getLogger(DashboardServiceImpl.class);

    @Autowired
    Gson gson;

    @Autowired
    ApiConnectionService apiConnection;

    @Override
    public DashboardResponse getData(String startDate, String endDate){
        DashboardRequest request = new DashboardRequest();
        request.setStart_date(startDate);
        request.setEnd_date(endDate);
        String response = apiConnection.sendRequest(gson.toJson(request), VariableString.dashboard);
        return gson.fromJson(response, DashboardResponse.class);
    }
}
