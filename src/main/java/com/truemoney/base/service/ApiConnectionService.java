package com.truemoney.base.service;

import com.truemoney.base.function.VariableString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;

@Configuration
public class ApiConnectionService {
    final static Logger logger = LoggerFactory.getLogger(ApiConnectionService.class);

    @Value("${api_url}")
    public String urlString; //Url Static

    @Value("${api_time_out}")
    public Integer apiTimeOut; //Url Static

    public String sendRequest(String data, String path) {
        logger.info("========[SEND REQUEST][BEGIN]=========" );

        String result = "";
        String output;
        StringBuffer sbf = new StringBuffer();

        try {
            URL url = new URL(urlString + path);

            logger.info("========[SEND REQUEST][URL]["+url+"]=========" );
            logger.info("[info] REQUEST :"+data );

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setConnectTimeout(apiTimeOut);
            conn.setReadTimeout(apiTimeOut);
            OutputStream os = conn.getOutputStream();
            os.write(data.getBytes());
            os.flush();

            Integer responseCode = conn.getResponseCode();

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            while ((output = br.readLine()) != null) {
                sbf.append(output);
            }

            result = sbf.toString();

            logger.info("[info] RESPONSE :"+result );
            logger.info("[info] RESPONSE CODE :"+responseCode );
            conn.disconnect();

        } catch (Exception e) {
            result = VariableString.JSON_ERROR;
            logger.error("[error] ERROR Exception :"+e );
            if (e instanceof ConnectException || e instanceof SocketTimeoutException) {

                logger.error("[error] ERROR TIMEOUT Exception :"+e );
                sbf = new StringBuffer();
                sbf.append("Timeout");
            }

        }
        logger.info("========[SEND REQUEST][END]===========\n" );
        return result;

    }
}
