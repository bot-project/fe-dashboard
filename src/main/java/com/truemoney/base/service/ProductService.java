package com.truemoney.base.service;

import com.truemoney.base.dto.response.BasicResponse;
import com.truemoney.base.dto.response.DashboardResponse;
import com.truemoney.base.dto.response.FeeSupplierResponse;

public interface ProductService {
    FeeSupplierResponse getData();
    FeeSupplierResponse.FeeSupplierDto getData(String id);
    BasicResponse updateData(FeeSupplierResponse.FeeSupplierDto feeSupplierDto);
}
