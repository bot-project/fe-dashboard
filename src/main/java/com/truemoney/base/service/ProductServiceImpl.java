package com.truemoney.base.service;

import com.google.gson.Gson;
import com.truemoney.base.dto.request.DashboardRequest;
import com.truemoney.base.dto.request.FeeSupplierRequest;
import com.truemoney.base.dto.response.BasicResponse;
import com.truemoney.base.dto.response.DashboardResponse;
import com.truemoney.base.dto.response.FeeSupplierResponse;
import com.truemoney.base.function.VariableString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl implements ProductService{
    private static final Logger logger = LoggerFactory.getLogger(ProductServiceImpl.class);

    @Autowired
    Gson gson;

    @Autowired
    ApiConnectionService apiConnection;

    @Override
    public FeeSupplierResponse getData(){
        DashboardRequest request = new DashboardRequest();
        String response = apiConnection.sendRequest(gson.toJson(request), VariableString.get_list_feesup);
        return gson.fromJson(response, FeeSupplierResponse.class);
    }

    @Override
    public FeeSupplierResponse.FeeSupplierDto getData(String id){
        FeeSupplierRequest request = new FeeSupplierRequest();
        request.setId_feesupplier(id);
        String response = apiConnection.sendRequest(gson.toJson(request), VariableString.get_feesup);
        return gson.fromJson(response, FeeSupplierResponse.FeeSupplierDto.class);
    }

    @Override
    public BasicResponse updateData(FeeSupplierResponse.FeeSupplierDto feeSupplierDto){
        String response = apiConnection.sendRequest(gson.toJson(feeSupplierDto), VariableString.update_feesup);
        return gson.fromJson(response, FeeSupplierResponse.class);
    }

}
