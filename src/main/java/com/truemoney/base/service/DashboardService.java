package com.truemoney.base.service;

import com.truemoney.base.dto.response.DashboardResponse;
import org.json.simple.JSONObject;

public interface DashboardService {
    DashboardResponse getData(String startDate, String endDate);
}
