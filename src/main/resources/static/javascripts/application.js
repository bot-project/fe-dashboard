/**
 * Created by Achmad Cahya Aditya on 11/28/15.
 */
$(document).ready(function(){
    //Loading Pages
    $(".button-click-overlay").click(function(){
        $(".container-overlay").LoadingOverlay("show");
    });
})
window.PixelAdmin.start(init);

function containerOverlay(){
    $(".container-overlay").LoadingOverlay("show");
}
function load(page,div){
    // var image_load = "<div style='margin-top: 30px; width: 100%;'><img style='display: block;margin-left: auto;margin-right: auto;' " +
    //     "class='img-responsive' width='100px' src='../../javascripts/loadingpage/loading.gif' th:src='@{/javascripts/loadingpage/loading.gif}' /></div>";
    jQuery.ajax({
        url: page,
        beforeSend: function(){
            // jQuery(div).html(image_load);
        },
        success: function(response){
            jQuery(div).html(response);
        },
        dataType:"html"
    });
    return false;
}

function loadModalForm(page,div){
    load(page, div);

    // LOADING THE AJAX MODAL
    jQuery( div ).modal( 'show', {
        backdrop: 'true'
    } );
}

function deletePopUp(message, formsubmit){
    bootbox.confirm({
        message: message,
        callback: function(result) {
            if(result){
                containerOverlay();
                $(formsubmit).submit();
            }
        },
        className: "bootbox-sm"
    });
}